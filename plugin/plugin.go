package plugin

import (
	"bufio"
	"os"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks the filename and content to make sure this is a Rails app
func Match(path string, info os.FileInfo) (bool, error) {
	if info.Name() == "Gemfile.lock" {
		// open file
		f, err := os.Open(path)
		if err != nil {
			return false, err
		}
		defer f.Close()

		// look for "rails" in DEPENDENCIES section
		scanner := bufio.NewScanner(f)
		inDependencies := false
		for scanner.Scan() {
			if strings.TrimSpace(scanner.Text()) == "DEPENDENCIES" {
				inDependencies = true
			}
			if inDependencies {
				fields := strings.Fields(scanner.Text())
				if len(fields) > 0 && fields[0] == "rails" {
					return true, nil
				}
			}
		}
	}
	return false, nil
}

func init() {
	plugin.Register("brakeman", Match)
}
