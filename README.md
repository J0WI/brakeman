# Brakeman analyzer

This analyzer is a wrapper around [Brakeman](https://brakemanscanner.org/),
an open source vulnerability scanner specifically designed for Ruby on Rails applications.
It's written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## How to update the underlying Scanner

**Warning:** the Brakeman scanner can no longer be updated [due to license update after acquisition](https://brakemanscanner.org/blog/2018/06/28/brakeman-has-been-acquired-by-synopsys/).

Issue to find a replacement: https://gitlab.com/gitlab-org/gitlab-ee/issues/7283

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see the [LICENSE](LICENSE) file.
