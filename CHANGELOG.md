# Brakeman analyzer changelog

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!21)

## v2.1.0
- Add support for custom CA certs (!18)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Bump brakeman to 4.3.1
- Don't detect a Rails application unless "rails" is a first level dependency
- Shows brakeman command error output
- Don't ignore Brakeman exit code

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
