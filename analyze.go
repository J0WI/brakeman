package main

import (
	"io"
	"os"
	"os/exec"
	"strconv"

	"github.com/urfave/cli"
)

const (
	flagConfidenceLevel = "confidence-level"

	pathOutput   = "/tmp/brakeman.json"
	pathBrakeman = "/usr/local/bundle/bin/brakeman"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.IntFlag{
			Name:   flagConfidenceLevel,
			Usage:  "Brakeman confidence level",
			EnvVar: "SAST_BRAKEMAN_LEVEL",
			Value:  1,
		},
	}
}
func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	level := strconv.Itoa(c.Int(flagConfidenceLevel))
	cmd := exec.Command(pathBrakeman, "-o", pathOutput, "-w", level,
		"--no-exit-on-warn", "--no-exit-on-error")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	/*
	   Exit codes as documented in lib/brakeman.rb:

	   #This exit code is used when warnings are found and the --exit-on-warn
	   #option is set
	   Warnings_Found_Exit_Code = 3

	   #Exit code returned when no Rails application is detected
	   No_App_Found_Exit_Code = 4

	   #Exit code returned when brakeman was outdated
	   Not_Latest_Version_Exit_Code = 5

	   #Exit code returned when user requests non-existent checks
	   Missing_Checks_Exit_Code = 6

	   #Exit code returned when errors were found and the --exit-on-error
	   #option is set
	   Errors_Found_Exit_Code = 7
	*/
	if err := cmd.Run(); err != nil {
		return nil, err
	}
	return os.Open(pathOutput)
}
