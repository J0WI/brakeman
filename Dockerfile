FROM ruby:2-stretch
# Do not upgrade brakman above 4.3.1: https://brakemanscanner.org/blog/2018/06/28/brakeman-has-been-acquired-by-synopsys/
RUN gem install brakeman -v 4.3.1
COPY /analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
